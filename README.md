# Advanced Lane Finding
## Udacity - Self-Driving Car NanoDegree - project 2

Photos and videos come from original [repo](https://github.com/udacity/CarND-Advanced-Lane-Lines).

[![Udacity - Self-Driving Car NanoDegree](https://s3.amazonaws.com/udacity-sdc/github/shield-carnd.svg)](http://www.udacity.com/drive)

Here is an example of output from the lane detector pipeline.

![lane_output](/steps/straight_lines1_output.jpg)

The pipeline uses all techniques covered during the SDCN course, but it is not
tweaked to work with harder challange videos (yet).

_It's assignment project... not a production code. :wink:_

Code
-------

- `calibrator.py`: video calibration pipeline
- `detector.py`: lane detector code with CLI frontend for single image detection
- `detector_params.py`: detector parameters
- `video_detector.py`: CLI frontend for video processing

Scripts
-------
- `calibrate.sh`: runs calibration pipline and generates `calibration_data.p`
  file
- `generate_output_images.sh`: runs lane detector on test images
- `generate_output_videos.sh`: runs lane detector on test videos
