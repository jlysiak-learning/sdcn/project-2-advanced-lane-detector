'''
    Camera calibration with chessboards.
    Copyrights (c) 2021 Jacek Łysiak
'''

import os
import glob
import argparse
import cv2
import logging
import numpy as np
import pickle

class Calibrator:

    _logger = None
    _calibration_data = None
    _orientations = None
    _calibration_images = []
    _good_images = []
    _obj_points = []
    _img_points = []
    _chessboard_dimensions = None
    _criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 30, 0.001)

    def __init__(self, images_paths, chessboard_dimensions):
        self._logger = logging.getLogger(self.__class__.__name__)
        self._calibration_images = images_paths
        self._chessboard_dimensions = chessboard_dimensions

    def _get_reference_points(self):
        nx, ny = self._chessboard_dimensions
        ref_points = np.zeros((nx * ny, 3), dtype=np.float32)
        ref_points[:,:2] = np.mgrid[0:nx,0:ny].T.reshape(-1,2)
        return ref_points

    def calibrate(self, save_images_to=None, preview=False):
        ref_points = self._get_reference_points()
        self._logger.debug(
            "Reference points: %s", [(p[0], p[1], p[2]) for p in ref_points])

        for img_path in self._calibration_images:
            self._logger.info('Processing image: %s', img_path)
            img = cv2.imread(img_path)
            gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
            ret, corners = cv2.findChessboardCorners(
                gray, self._chessboard_dimensions, None)
            if not ret:
                self._logger.warning('Nothing found on: %s', img_path)
                continue
            # 11,11 winSize, -1-1, zero zone
            corners2 = cv2.cornerSubPix(gray, corners, (11,11), (-1,-1), self._criteria)
            self._img_points.append(corners2)
            self._obj_points.append(ref_points)
            self._good_images.append(img_path)
            if preview:
                img = cv2.drawChessboardCorners(
                    img, self._chessboard_dimensions, corners2, ret)
                cv2.imshow('img',img)
                cv2.waitKey(0)

        ret, mtx, dist, rvecs, tvecs = cv2.calibrateCamera(
            self._obj_points, self._img_points, gray.shape[::-1], None, None)
        if not ret:
            raise RuntimeError("Calibration failed!")

        coeffs_names = ['k1', 'k2', 'p1', 'p2', 'k3']
        coeffs_desc = ['%s=%f' % (k, v) for k, v in zip(coeffs_names, dist.reshape(-1))]
        self._logger.info('Distortions coefficients:\n' + ', '.join(coeffs_desc))
        self._logger.info('Camera matrix:\n%s', mtx)

        self._orientations = rvecs, tvecs
        self._calibration_data = mtx, dist
        return self._calibration_data, self._good_images

    def reprojection_error(self):
        if self._calibration_data is None:
            raise RuntimeError("First you need to calibrate your camera!")
        mtx, dist = self._calibration_data
        rvecs, tvecs = self._orientations
        total_err = 0
        for obj_pts, img_pts, rvec, tvec in zip(
                self._obj_points, self._img_points, rvecs, tvecs):
            prj_pts, _ = cv2.projectPoints(obj_pts, rvec, tvec, mtx, dist)
            total_err += cv2.norm(img_pts, prj_pts, cv2.NORM_L2) / len(prj_pts)
        return total_err / len(obj_pts)

    def save_images(self, output_dir):
        if self._calibration_data is None:
            raise RuntimeError("First you need to calibrate your camera!")
        if not os.path.isdir(output_dir):
            os.makedirs(output_dir)
        mtx, dist = self._calibration_data
        rvecs, tvecs = self._orientations
        for img_path, obj_pts, img_pts, rvec, tvec in zip(
                self._good_images, self._obj_points,
                self._img_points, rvecs, tvecs):
            img = cv2.imread(img_path)
            img = cv2.drawChessboardCorners(
                img, self._chessboard_dimensions, img_pts, True)
            prj_pts, _ = cv2.projectPoints(obj_pts, rvec, tvec, mtx, dist)
            for pt in prj_pts:
                pt = pt[0]
                cv2.circle(img, (int(pt[0]), int(pt[1])), 2, (255,0,255), 3)
            path = os.path.join(output_dir, os.path.basename(img_path))
            self._logger.debug("Saving file: %s", path)
            cv2.imwrite(path, img)
            img_un = cv2.undistort(img, mtx, dist, None, None)
            path = os.path.join(output_dir,
                os.path.splitext(os.path.basename(img_path))[0] + '_un.jpg')
            cv2.imwrite(path, img_un)

def set_logger(lvl):
    level=logging.WARNING
    if lvl == 1:
        level=logging.INFO
    elif lvl > 1:
        level=logging.DEBUG
    fmt = "%(name)s:%(levelname)s] %(message)s"
    logging.basicConfig(level=level, format=fmt)

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('nx', type=int)
    parser.add_argument('ny', type=int)
    parser.add_argument('output_file')
    parser.add_argument('calibration_images', nargs='*')
    parser.add_argument('-v', action='count', default=0)
    parser.add_argument('--save_images_to')
    parser.add_argument('--preview', action='store_true')

    args = parser.parse_args()
    for p in args.calibration_images:
        assert os.path.isfile(p)

    set_logger(args.v)

    calibrator = Calibrator(args.calibration_images, (args.nx, args.ny))
    calibration_data, good_images = calibrator.calibrate(preview=args.preview)
    mtx, dist = calibration_data
    repr_error = calibrator.reprojection_error()
    logging.info("Mean reprojection error: %f" % repr_error)
    if args.save_images_to:
        calibrator.save_images(output_dir=args.save_images_to)

    cam_data = dict(mtx=mtx, dist=dist)
    with open(args.output_file, 'wb') as f:
        pickle.dump(cam_data, f)
    logging.info("Camera calibration pickled into: %s", args.output_file)
