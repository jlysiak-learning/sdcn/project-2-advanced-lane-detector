'''
    Lane detector
    Copyright (c) 2021 Jacek Łysiak
'''

import os
import argparse
import logging
import pickle
import numpy as np
import cv2

from detector_params import DetectorParams

class Filter:
    def __init__(self, coeff):
        self._coeff = coeff
        self._values = None
        self._cnt = 0

    def update(self, *vals):
        if self._cnt:
            self._values = (1 - self._coeff) * self._values + self._coeff * np.array(vals)
        else:
            self._values = np.array(vals)
        self._cnt += 1
        return tuple(self._values)

class Detector:
    '''Lane detector on image.'''

    _logger = None
    _mtx = None
    _dist = None
    _images = []
    _img_shape = None
    _save_steps = None

    _last_fit = None
    _filter_l = None
    _filter_r = None

    def __init__(self, mtx, dist, params, save_steps=False):
        self._logger = logging.getLogger(self.__class__.__name__)
        self._mtx, self._dist = mtx, dist
        self._params = params
        self._save_steps = save_steps
        self._filter_l = Filter(coeff=params.filter_coeff)
        self._filter_r = Filter(coeff=params.filter_coeff)
        self._logger.debug('Camera matrix: %s', self._mtx)
        self._logger.debug('Camera coeffs: %s', self._dist)

    def detect_lane(self, img, use_prev_fit=False):
        '''Find lane (left and right line) on a BGR image.'''
        self._img_shape = img.shape[:2][::-1]
        self._images = []

        self._store_image('original', img)
        img_undistorted = self._undistort(img)
        img_roi = self._show_roi(img_undistorted)
        self._store_image('undistorted', img_roi)
        img_binary = self._thresholding(img_undistorted)
        self._store_image('binary_map', img_binary * 255)
        img_top = self._top_view(img_binary)
        self._store_image('top_view', img_top * 255)
        points, lines_mask, rects = self._find_lines(img_top)
        fitted_lines, fitted_lines_mask = self._fit_lines(*points)
        img_prev = self._prev_fitted_lines(
            img_top, lines_mask,
            fitted_lines_mask, rects)
        self._store_image('lines', img_prev)
        position, radius = self._compute_pos_rad(*fitted_lines)
        img = self._draw_hud(img_undistorted, position, radius, lines_mask, fitted_lines)
        self._store_image('output', img)
        return img

    def _put_text(self, img, text, x, y):
        font = cv2.FONT_HERSHEY_SIMPLEX
        org = (x, y)
        fontScale = 1
        color = (200, 244, 255)
        thickness = 2
        return cv2.putText(img, text, org, font, fontScale, color, thickness, cv2.LINE_AA)

    def _draw_hud(self, img, position, radius, lines_mask, fitted_lines):
        sx, sy = tuple(self._img_shape)
        text = 'Position: %.2fm ' % abs(position)
        text += '(left)' if position < 0 else '(right)'
        img = self._put_text(img, text, 10, 100)
        text = 'Radius: %.0fm ' % abs(radius)
        img = self._put_text(img, text, 10, 160)
        lines_mask = self._transform_back(lines_mask.astype(np.uint8))

        lf, rf, flxs, frxs, ys = fitted_lines
        lane_mask = np.zeros((sy, sx), dtype=np.uint8)
        verts = np.concatenate([ np.dstack((flxs, ys)), np.dstack((frxs, ys))[:,::-1,:] ], axis=1)
        cv2.fillPoly(lane_mask, verts, 255)
        lane_mask = self._transform_back(lane_mask)
        img = img.astype(np.float64)
        self._overlay(img, lane_mask, (0, 255, 0), 0.3)
        self._overlay(img, lines_mask, (0, 0, 255), 0.7)
        return img.astype(np.uint8)

    def _overlay(self, img, mask, col, alpha):
        img[mask > 0] *= (1 - alpha)
        img[mask > 0] += alpha * np.array(col)


    def _compute_pos_rad(self, lf, rf, flxs, frxs, ys):
        la, lb, lc = tuple(lf)
        ra, rb, rc = tuple(rf)
        self._logger.debug('Radius left, params: a=%f b=%f c=%f', la, lb, lc)
        self._logger.debug('Radius right, params: a=%f b=%f c=%f', ra, rb, rc)
        lr = self._radius(la, lb, ys[-1])
        rr = self._radius(ra, rb, ys[-1])
        r = (lr + rr) * 0.5
        self._logger.debug('Radius: Rl=%f Rr=%f Rmean=%f', lr, rr, r)
        offset = self._offset(flxs, frxs)
        self._logger.debug('Offset: %fm', offset)
        return offset, r

    def _offset(self, lxs, rxs):
        off = self._img_shape[0] * 0.5 - (lxs[-1] + rxs[-1]) * 0.5
        return off * self._params.mx

    def _radius(self, a, b, y):
        mx = self._params.mx
        my = self._params.my
        return ((1 + ((2 * a * y + b) * mx / my ) ** 2) ** 1.5) / abs(2 * a * mx
                / my / my)

    def _prev_fitted_lines(self, img, lines_mask, fitted_lines_mask, rects):
        img_prev = np.dstack([img, img, img]) * 255
        for l in rects:
            if not l:
                continue
            for tl, rb in l:
                cv2.rectangle(img_prev, tl, rb, (0, 100, 200), 2)
        img_prev[lines_mask] = [100, 150, 100]
        img_prev[fitted_lines_mask] = [0, 0, 255]
        return img_prev


    def _fit_lines(self, lxs, lys, rxs, rys):
        sx, sy = tuple(self._img_shape)
        lf = np.polyfit(lys, lxs, 2)
        rf = np.polyfit(rys, rxs, 2)
        ys = np.linspace(0, sy-1, sy)

        la, lb, lc = self._filter_l.update(*tuple(lf))
        ra, rb, rc = self._filter_r.update(*tuple(rf))

        flxs = la * ys ** 2 + lb * ys + lc
        frxs = ra * ys ** 2 + rb * ys + rc

        flxs = flxs.astype(np.int32)
        frxs = frxs.astype(np.int32)
        ys = ys.astype(np.int32)
        self._last_fit = (la, lb, lc), (ra, rb, rc)

        mask = np.zeros((sy, sx), dtype=bool)
        xmask = (flxs < sx) & (flxs >= 0) & (frxs < sx) & (frxs >= 0)
        flxs = flxs[xmask]
        frxs = frxs[xmask]
        ys = ys[xmask]
        mask[ys, flxs] = True
        mask[ys, frxs] = True
        return (lf, rf, flxs, frxs, ys), mask

    def _find_lines(self, img):
        if self._last_fit is None:
            # Run lines from the beginning
            nwindows = 9
            margin = 100
            minpix = 50
            sx, sy = tuple(self._img_shape)
            self._logger.debug("FindLanes: img size=(%d, %d)", sx, sy)

            hist_y_start = sy * 3 // 4
            hist = np.sum(img[hist_y_start:,:], axis=0)
            mid = int(sx // 2)
            leftx_base = np.argmax(hist[:mid])
            rightx_base = np.argmax(hist[mid:]) + mid
            self._logger.debug(
                "FindLanes: midx=%d, leftx_base=%d, rightx_base=%d",
                mid, leftx_base, rightx_base)

            wnd_h = int(sy // nwindows)
            nonzero = img.nonzero()
            nonzeroy = nonzero[0]
            nonzerox = nonzero[1]

            leftx_current = leftx_base
            rightx_current = rightx_base
            left_lane_inds = []
            right_lane_inds = []
            rectsl = []
            rectsr = []

            for i in range(nwindows):
                y_low = sy - (i+1) * wnd_h
                y_high = sy - i * wnd_h
                xleft_low = leftx_current - margin
                xleft_high = leftx_current + margin
                xright_low = rightx_current - margin
                xright_high = rightx_current + margin
                self._logger.debug(
                    "FindLanes: wnd=%d, yl=%d, yh=%d, leftxl=%d, leftxh=%d, "
                    "rightxl=%d, rightxh=%d", i, y_low, y_high, xleft_low,
                    xleft_high, xright_low, xright_high)
                rectsl.append(((xleft_low, y_low), (xleft_high, y_high)))
                rectsr.append(((xright_low, y_low), (xright_high, y_high)))

                good_left_inds = ((nonzeroy >= y_low) & (nonzeroy < y_high) & \
                    (nonzerox >= xleft_low) & (nonzerox < xleft_high)).nonzero()[0]
                good_right_inds = ((nonzeroy >= y_low) & (nonzeroy < y_high) & \
                    (nonzerox >= xright_low) & (nonzerox < xright_high)).nonzero()[0]
                left_lane_inds.append(good_left_inds)
                right_lane_inds.append(good_right_inds)
                if len(good_left_inds) > minpix:
                    leftx_current = int(nonzerox[good_left_inds].mean())
                if len(good_right_inds) > minpix:
                    rightx_current = int(nonzerox[good_right_inds].mean())
            l_inds = np.concatenate(left_lane_inds)
            r_inds = np.concatenate(right_lane_inds)
        else:
            (la, lb, lc), (ra, rb, rc) = self._last_fit
            nonzero = img.nonzero()
            nonzeroy = nonzero[0]
            nonzerox = nonzero[1]
            margin = 50
            lxs = nonzeroy**2 * la + nonzeroy * lb + lc
            rxs = nonzeroy**2 * ra + nonzeroy * rb + rc
            l_inds = ((nonzerox >= lxs - margin) & (nonzerox <= lxs + margin)).nonzero()[0]
            r_inds = ((nonzerox >= rxs - margin) & (nonzerox <= rxs + margin)).nonzero()[0]
            rectsl, rectsr = None, None

        lxs = nonzerox[l_inds]
        lys = nonzeroy[l_inds]
        rxs = nonzerox[r_inds]
        rys = nonzeroy[r_inds]
        mask = np.zeros_like(img, dtype=bool)
        mask[lys, lxs] = True
        mask[rys, rxs] = True
        return (lxs, lys, rxs, rys), mask, (rectsl, rectsr)




    def _show_roi(self, img):
        roi_vertices = self._roi_vertices
        img = img.copy()
        for i, j in zip(range(-1, 3), range(0, 4)):
            p1 = tuple(map(int, roi_vertices[0][i]))
            p2 = tuple(map(int, roi_vertices[0][j]))
            cv2.line(img, p1, p2, (0,0,255), 2)
        return img

    def _top_view(self, img):
        M, _ = self._perspective_transforms_mtxs
        return self._perspective_transform(img, M)

    def _transform_back(self, img):
        _, Minv = self._perspective_transforms_mtxs
        return self._perspective_transform(img, Minv)

    def _perspective_transform(self, img, M):
        return cv2.warpPerspective(
            img, M, self._img_shape, flags=cv2.INTER_LINEAR)

    def _undistort(self, img):
        return cv2.undistort(img, self._mtx, self._dist, None, None)

    def _thresholding(self, img, store_images=False):
        img_hls = cv2.cvtColor(img, cv2.COLOR_BGR2HLS)
        channel_s = img_hls[:,:,2]  # Use S channel
        channel_r = img[:,:,2]  # Use R channel

        value_s_bin = self._apply_threshold(channel_s, self._params.value_s_threshold)
        value_r_bin = self._apply_threshold(channel_r, self._params.value_r_threshold)

        sobelx_s, sobely_s, sobeld_s = self._sobel(channel_s)
        sobelx_r, sobely_r, sobeld_r = self._sobel(channel_r)
        sobelx_s_bin = self._apply_threshold(sobelx_s, self._params.sobel_x_threshold)
        sobely_s_bin = self._apply_threshold(sobely_s, self._params.sobel_y_threshold)
        sobeld_s_bin = self._apply_threshold(sobeld_s, self._params.sobel_d_threshold)

        sobelx_r_bin = self._apply_threshold(sobelx_r, self._params.sobel_x_threshold)
        sobely_r_bin = self._apply_threshold(sobely_r, self._params.sobel_y_threshold)
        sobeld_r_bin = self._apply_threshold(sobeld_r, self._params.sobel_d_threshold)


        if self._save_steps:
            # Additional if to not perform too many ops
            self._store_image('s_channel', channel_s)
            self._store_image('r_channel', channel_r)
            self._store_image('s_value_bin', value_s_bin * 255)
            self._store_image('r_value_bin', value_r_bin * 255)
            self._store_image('s_sobelx_bin', sobelx_s_bin * 255)
            self._store_image('s_sobely_bin', sobely_s_bin * 255)
            self._store_image('s_sobeld_bin', sobeld_s_bin * 255)
            self._store_image('r_sobelx_bin', sobelx_r_bin * 255)
            self._store_image('r_sobely_bin', sobely_r_bin * 255)
            self._store_image('r_sobeld_bin', sobeld_r_bin * 255)

        #combined = sobelx_s_bin + sobely_s_bin + sobeld_s_bin + \
        #    sobelx_r_bin + sobely_r_bin + sobeld_r_bin
        combined = sobelx_s_bin + sobely_s_bin + sobelx_r_bin + sobely_r_bin + \
            value_s_bin + value_r_bin
        combined = combined.astype(np.float32)
        combined /= 6.
        # Get pixels that were set at many binary masks
        # We are effectively mixing different maps instead of doing just AND
        # that would lead to a big rejection rate.
        combined_bin = self._apply_threshold(
                combined,
                (self._params.binary_mask_combined_threshold, 1))
        return combined_bin

    def _sobel(self, img):
        '''Get x, y gradients and it's direction. '''
        sobel_kernel = self._params.sobel_kernel
        sobelx = cv2.Sobel(img, cv2.CV_64F, 1, 0, ksize=sobel_kernel) # take x dir
        sobely = cv2.Sobel(img, cv2.CV_64F, 0, 1, ksize=sobel_kernel) # take x dir
        sobelx_abs = np.absolute(sobelx)
        sobely_abs = np.absolute(sobely)
        sobeld_abs = np.arctan2(sobely_abs, sobelx_abs)
        sobelx_norm = np.uint8(255 * sobelx_abs / np.max(sobelx_abs))
        sobely_norm = np.uint8(255 * sobely_abs / np.max(sobely_abs))
        return sobelx_norm, sobely_norm, sobeld_abs

    def _apply_threshold(self, img, th):
        binmap = np.zeros_like(img)
        binmap[(img > th[0]) & (img < th[1])] = 1
        return binmap

    @property
    def _roi_vertices(self):
        return self._params.roi * self._img_shape

    @property
    def _roi_vertices_warpped(self):
        return self._params.roi_warpped * self._img_shape

    @property
    def _perspective_transforms_mtxs(self):
        roi_vertices = self._roi_vertices.astype(np.float32)
        roi_vertices_warpped = self._roi_vertices_warpped.astype(np.float32)
        M = cv2.getPerspectiveTransform(roi_vertices, roi_vertices_warpped)
        Minv = cv2.getPerspectiveTransform(roi_vertices_warpped, roi_vertices)
        return M, Minv

    @property
    def steps(self):
        return self._images

    def _store_image(self, name, img):
        if self._save_steps:
            if len(img.shape) < 3:
                img = np.dstack([img, img, img])
                self._images.append((name, img))
            else:
                self._images.append((name, img.copy()))


def set_logger(lvl):
    level=logging.WARNING
    if lvl == 1:
        level=logging.INFO
    elif lvl > 1:
        level=logging.DEBUG
    fmt = "%(name)s:%(levelname)s] %(message)s"
    logging.basicConfig(level=level, format=fmt)

def put_text(img, text):
    font = cv2.FONT_HERSHEY_SIMPLEX
    org = (00, 50)
    fontScale = 1
    color = (0, 255, 255)
    thickness = 2
    cv2.putText(img, text, org, font, fontScale, color, thickness, cv2.LINE_AA)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('camera_calibration')
    parser.add_argument('input_images', nargs='*')
    parser.add_argument('--preview_steps', action='store_true')
    parser.add_argument('--preview_output', action='store_true')
    parser.add_argument('--save_steps_to')
    parser.add_argument('--save_output_to')
    parser.add_argument('-v', action='count', default=0)

    args = parser.parse_args()
    for p in args.input_images:
        assert os.path.isfile(p)
    if args.save_steps_to:
        if not os.path.isdir(args.save_steps_to):
            os.makedirs(args.save_steps_to)
    if args.save_output_to:
        if not os.path.isdir(args.save_output_to):
            os.makedirs(args.save_output_to)

    set_logger(args.v)
    with open(args.camera_calibration, 'rb') as f:
        cam_calib = pickle.load(f)
    params = DetectorParams()
    save_steps = args.preview_steps or args.save_steps_to
    detector = Detector(cam_calib['mtx'], cam_calib['dist'], params, save_steps)

    for p in args.input_images:
        img = cv2.imread(p)
        output = detector.detect_lane(img, use_prev_fit=False)
        if args.save_output_to:
            path = os.path.join(args.save_output_to, os.path.basename(p))
            cv2.imwrite(path, img)
        if args.preview_output:
            cv2.imshow('prev', img)
            cv2.waitKey(0)
        if save_steps:
            for name, img in detector.steps:
                put_text(img, name)
                if args.preview_steps:
                    cv2.imshow('prev', img)
                    cv2.waitKey(0)
                if args.save_steps_to:
                    fname = os.path.splitext(os.path.basename(p))[0]
                    path = os.path.join(args.save_steps_to, fname + '_' + name + '.jpg')
                    cv2.imwrite(path, img)
