'''
    Lane detector parameters
    Copyright (c) 2021 Jacek Łysiak
'''

import numpy as np


class DetectorParams:
    # ROI params
    # Relative parameters, as image sizes may vary
    # NOTE: Set to match lanen width
    roi_middle_x_width = 0.075
    roi_bottom_x_width = 0.70
    roi_middle_y_shift = 0.13
    roi_bottom_y_shift = 0.0
    @property
    def roi(self):
        # fillPoly requres list of polygons, so list, of lists of points is required
        return np.array([[
                (0.5 - self.roi_bottom_x_width / 2, 1 - self.roi_bottom_y_shift),
                (0.5 - self.roi_middle_x_width / 2, 0.5 + self.roi_middle_y_shift),
                (0.5 + self.roi_middle_x_width / 2, 0.5 + self.roi_middle_y_shift),
                (0.5 + self.roi_bottom_x_width / 2, 1 - self.roi_bottom_y_shift),
            ]],
            dtype=np.float32)

    roi_warpped_width = 0.5
    @property
    def roi_warpped(self):
        return np.array([[
                (0.5 - self.roi_warpped_width / 2, 1),
                (0.5 - self.roi_warpped_width / 2, 0),
                (0.5 + self.roi_warpped_width / 2, 0),
                (0.5 + self.roi_warpped_width / 2, 1),
            ]],
            dtype=np.float32)

    sobel_kernel = 3
    value_s_threshold = (180, 255)
    value_r_threshold = (200, 255)
    sobel_x_threshold = (30, 130)
    sobel_y_threshold = (30, 130)
    sobel_d_threshold = (0.8, 1.1)
    binary_mask_combined_threshold = 1/6

    mx = 3.7 / 700
    my = 30 / 720

    filter_coeff = 0.3
