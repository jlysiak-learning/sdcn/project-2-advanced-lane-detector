'''
    Lane detector
    Copyright (c) 2021 Jacek Łysiak
'''

import os
import argparse
import logging
import pickle
import numpy as np
import cv2
from moviepy.editor import VideoFileClip

from detector_params import DetectorParams
from detector import Detector

def set_logger(lvl):
    level=logging.WARNING
    if lvl == 1:
        level=logging.INFO
    elif lvl > 1:
        level=logging.DEBUG
    fmt = "%(name)s:%(levelname)s] %(message)s"
    logging.basicConfig(level=level, format=fmt)

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('camera_calibration')
    parser.add_argument('input_videos', nargs='*')
    parser.add_argument('--preview', action='store_true')
    parser.add_argument('--start_time', type=int)
    parser.add_argument('--stop_time', type=int)
    parser.add_argument('--save_output_to')
    parser.add_argument('-v', action='count', default=0)

    args = parser.parse_args()
    for p in args.input_videos:
        assert os.path.isfile(p)
    if args.save_output_to:
        if not os.path.isdir(args.save_output_to):
            os.makedirs(args.save_output_to)

    set_logger(args.v)

    with open(args.camera_calibration, 'rb') as f:
        cam_calib = pickle.load(f)

    params = DetectorParams()

    for p in args.input_videos:
        with VideoFileClip(p) as clip:
            logging.info('Processing video: %s', p)
            detector = Detector(cam_calib['mtx'], cam_calib['dist'], params, save_steps=False)
            fn = lambda img: detector.detect_lane(img, use_prev_fit=True)
            output_clip = clip.fl_image(fn)
            if args.preview:
                output_clip.preview()
            if args.save_output_to:
                path = os.path.join(args.save_output_to, os.path.basename(p))
                logging.info('Saving clip as: %s', path)
                output_clip.write_videofile(path, audio=False)
                logging.info('Saving finished!')
