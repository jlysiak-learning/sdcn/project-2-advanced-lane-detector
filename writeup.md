# Writeup

---

The goals / steps of this project are the following:

* Compute the camera calibration matrix and distortion coefficients given a set of chessboard images.
* Apply a distortion correction to raw images.
* Use color transforms, gradients, etc., to create a thresholded binary image.
* Apply a perspective transform to rectify binary image ("birds-eye view").
* Detect lane pixels and fit to find the lane boundary.
* Determine the curvature of the lane and vehicle position with respect to center.
* Warp the detected lane boundaries back onto the original image.
* Output visual display of the lane boundaries and numerical estimation of lane curvature and vehicle position.

[//]: # (Image References)

[cal1]: ./calibration_preview/calibration2.jpg "Calibration example"
[cal1_un]: ./calibration_preview/calibration2_un.jpg "Calibration example undistorted"

[lane_binmap]: ./steps/straight_lines1_binary_map.jpg
[lane_lines]: ./steps/straight_lines1_lines.jpg
[lane_orig]: ./steps/straight_lines1_original.jpg
[lane_output]: ./steps/straight_lines1_output.jpg
[lane_rch]: ./steps/straight_lines1_r_channel.jpg
[lane_rsobd]: ./steps/straight_lines1_r_sobeld_bin.jpg
[lane_rsobx]: ./steps/straight_lines1_r_sobelx_bin.jpg
[lane_rsoby]: ./steps/straight_lines1_r_sobely_bin.jpg
[lane_rval]: ./steps/straight_lines1_r_value_bin.jpg
[lane_sch]: ./steps/straight_lines1_s_channel.jpg
[lane_ssobd]: ./steps/straight_lines1_s_sobeld_bin.jpg
[lane_ssobx]: ./steps/straight_lines1_s_sobelx_bin.jpg
[lane_ssoby]: ./steps/straight_lines1_s_sobely_bin.jpg
[lane_sval]: ./steps/straight_lines1_s_value_bin.jpg
[lane_top]: ./steps/straight_lines1_top_view.jpg
[lane_un]: ./steps/straight_lines1_undistorted.jpg

## Camera Calibration

### Intro

The camera calibration code is in `calibrator.py` file. It provides the calibration pipeline code as well as CLI frontend.

Usage of the pipeline is shown in `calibrate.sh` script:
> python3 calibrator.py --save_images_to=calibration_preview -v 9 6 calibration_data.p camera_cal/*

It takes all images calibration (`camera_cal/*`), the chessboard dimensions (`9 x 6`)
and saves image previews into `calibration_preview` directory. The preview images
help with validation if the pipeline is working correctly, like the picture below.

![alt][cal1]

Color points connected together are the points drawn by `drawChessboardCorners`
(line 99) and the pink points are object points (XY grid) projected back on the
image plane to verify if the calibration procedure.

Calibration data are pickled into `calibration_data.p` file as `numpy` arrays.

### The pipeline

1. List of calibration images paths is passed as argument to Calibrator class
   (line 138).
2. Calibration pipeline is executed (line 139).
3. First generate reference points. Its a list of points `(i, j, 0)` where `i: 0 <= i < nx`
  and `j: 0 <= j < n`
4. Process images in the loop
  - Open image and convert to grayscale.
  - Run `findChessboardCorners` (line 46) to find inner corners on the image.
  - If no corners have been found, skip the image.
  - If found, refine corners position using `cornerSubPix` (line 52).
  - Append refined corners and reference points to appropriate lists
     (`_img_points` and `_obj_points`).
5. Run `calibrateCamera` on all reference points and image points pairs (line
   62). Save rotation and translation vectors to be able to project reference
   points back on the image plane.
6. Compute reprojection error by projecting reference points on the image plane
  and computing L2 distance between corners on the image and projected points.
  (line 84)
7. At end save calibration preview (draw corners, projected points) on the
   original image (lines 99, 104) and undistorted image (line 108)

### Examples:

Distorted: ![alt][cal1]
Undistorted: ![alt][cal1_un]

## Lane detection pipeline

Main lane detector code is in `detector.py` file.

1. Create detector object. Provide camera params (loaded from file) and detector params (imported
   from `detector_params` module) (line 409).
2. Run detector on an image (line 413). ![alt][lane_orig]
3. Undistort image (line 270) ![alt][lane_un]
4. Apply thresholding on undistored image to receive binary map (line 273)
   - extract R channel from BGR image ![alt][lane_rch]
   - convert BGR image to HLS
   - extract S channel from HLS image ![alt][lane_sch]
   - apply R and S channel value thresholds (lines 279, 280) ![alt][lane_sval] ![alt][lane_rval]
   - compute gradients (X, Y) of S/R channels using Sobel operators and apply
     thresholding (line 282-290) ![alt][lane_ssobx] ![alt][lane_ssoby] ![alt][lane_rsobx] ![alt][lane_rsoby]
   - combine all binary maps, compute their mean, and apply thresholding to
     extract the final binary mask (line 308, 311) ![alt][lane_binmap]
5. Warp the binary map and create `bird's eye view` (line 261). Perspective
   transform matrices are computed at line 349.
   
   Points used to compute perspective transform are as follows:
```
    roi_middle_x_width = 0.075
    roi_bottom_x_width = 0.70
    roi_middle_y_shift = 0.13
    roi_bottom_y_shift = 0.0
    @property
    def roi(self):
        # fillPoly requres list of polygons, so list, of lists of points is required
        return np.array([[
                (0.5 - self.roi_bottom_x_width / 2, 1 - self.roi_bottom_y_shift),
                (0.5 - self.roi_middle_x_width / 2, 0.5 + self.roi_middle_y_shift),
                (0.5 + self.roi_middle_x_width / 2, 0.5 + self.roi_middle_y_shift),
                (0.5 + self.roi_bottom_x_width / 2, 1 - self.roi_bottom_y_shift),
            ]],
            dtype=np.float32)

    roi_warpped_width = 0.5
    @property
    def roi_warpped(self):
        return np.array([[
                (0.5 - self.roi_warpped_width / 2, 1),
                (0.5 - self.roi_warpped_width / 2, 0),
                (0.5 + self.roi_warpped_width / 2, 0),
                (0.5 + self.roi_warpped_width / 2, 1),
            ]],
            dtype=np.float32)
```
  They are expressed in relative coordinates. The `roi_*` params has been chosed
  to fit lane. ![alt][lane_un]

  Transformation has been verified by checking
  if warpped image (top view) contains two almost parallel lines. ![alt][lane_top]

6. Lines detection is in `_find_lines` method (line 172). If there is no fit
   data, the sliding window method is used to identyfy left and right lines.

   To selected points a second order polynomial is fitted (line 146).
   ![alt][lane_lines]

   Obtained A, B, C params are filtered using a low-pass filter.

7. I used obtained A, B, C params to compute radius of track curvature using the
   following equation:
```
    def _radius(self, a, b, y):
        mx = self._params.mx
        my = self._params.my
        return ((1 + ((2 * a * y + b) * mx / my ) ** 2) ** 1.5) / abs(2 * a * mx
                / my / my)
```
  where `mx` and `my` are x and y scale defined in `detector_params.py:48`

  Car position has been computed by taking the lowest points (y=719) of fitted
  curves and assuming the car is in the middle of the image.
```
  def _offset(self, lxs, rxs):
      off = self._img_shape[0] * 0.5 - (lxs[-1] + rxs[-1]) * 0.5
      return off * self._params.mx
```
8. Obtained results are warpped back to undistorted image (line73).
   ![alt][lane_output]

## Video pipeline

Video processor is in the `video_detector.py` file. It contains CLI frontend.
It uses `moviepy.editor` module to process viedo frames. Detector uses fitted
curved in a previous frame to quickly find a next line on the subsequent image.

Here's a [link to my video result](./output_videos/project_video.mp4)

### Discussion

The pipeline fails on challange videos.
Color thrsholding might be better, as it does not detect very thin line on the
challange video.

The next thing is regulation of a field of view. In the hardest challange the
algorithm tries to fit points that are not the lines (i.e a part of
motorcyclist)
